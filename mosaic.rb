#!/usr/bin/env ruby

require 'optparse'
require 'chunky_png'

class Mosaic
  def process(options, filename, available_colours)
    # required dimensions for the image (number of 1x1 bricks)
    image_width = options[:plate_size] * options[:width] / options[:input_files].length
    image_height = options[:plate_size] * options[:height]

    # load the image and scale/crop as necessary
    image = load_image(options, image_width, image_height, filename)
    find_closest_colours(image, available_colours)
  end

  def load_image(options, required_width, required_height, filename)
    puts("required size of #{filename} #{required_width} x #{required_height}") if options[:verbose]
    image = ChunkyPNG::Image.from_file(filename)
    if image.width < required_width || image.height < required_height
      puts "Image #{filename} too small (#{image.width} x #{image.height})"
      exit 1
    end

    # find the ideal size of the image
    scale_width = required_width
    scale_height = required_height

    scale = required_width.to_f / image.width
    height_if_scaled = image.height * scale
    if height_if_scaled > required_height
      scale_height = height_if_scaled.to_i
    else
      scale = required_height.to_f / image.height
      scale_width = (image.width * scale).to_i
    end

    # scale image
    image.resample_nearest_neighbor!(scale_width, scale_height)

    # now crop
    crop_x = ((image.width - required_width) / 2.0).to_i
    crop_y = ((image.height - required_height) / 2.0).to_i
    image.crop!(crop_x, crop_y, required_width, required_height)

    @image = image
  end

  def find_closest_colours(input_image, available_colours)
    @used_colours = {}
    @colour_places = []
    @output_image = ChunkyPNG::Image.new(input_image.width, input_image.height)
    (0...input_image.width).each {|x|
      (0...input_image.height).each {|y|
        colour = input_image[x, y]
        closest = closest(colour, available_colours)
        @output_image[x, y] = closest

        if @used_colours.has_key?(closest)
          @used_colours[closest] = @used_colours[closest] + 1
        else
          @used_colours[closest] = 1
        end
      }
    }
  end

  def used_colours
    @used_colours
  end

  def output_image
    @output_image
  end

  def save_output(filename)
    @output_image.save(filename)
  end

  def closest(colour, available_colours)
    best_distance = Float::MAX
    result = nil

    available_colours.each {|c|
      distance = ChunkyPNG::Color.euclidean_distance_rgba(c, colour)
      if distance < best_distance
        result = c
        best_distance = distance
      end
    }
    result
  end

  def join(mosaic1, mosaic2)
    # check images are compatible
    image1 = mosaic1.output_image
    image2 = mosaic2.output_image
    if image1.width != image2.width ||
        image1.height != image2.height
      puts 'Joining incompatible images. Unexpected.'
      exit
    end

    # image
    @output_image = ChunkyPNG::Image.new(image1.width + image2.width, image1.height)
    (0...image1.width).each {|x|
      (0...image1.height).each {|y|
        @output_image[x*2, y] = image1[x, y]
        @output_image[(x*2)+1, y] = image2[x, y]
      }
    }

    # used colours
    @used_colours = mosaic1.used_colours.clone
    mosaic2.used_colours.each {|colour,count|
      if @used_colours.has_key?(colour)
        @used_colours[colour] = @used_colours[colour] + count
      else
        @used_colours[colour] = count
      end
    }
  end

  def report(options)
    # output list of colours, with the letter used to identify each one
    palette_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.chars
    palette_key = {}
    puts "\nColours:"
    @used_colours.sort_by {|_,count| count}.reverse.each {|colour,count|
      colour_name = lego_colours.key(colour)
      letter = palette_letters.delete_at(0)
      palette_key[colour] = letter
      puts "#{letter}: #{colour_name} (#{count})"
    }

    # now the arrangement of baseplates (if there are more than one)
    multi_plate = options[:width]> 1 && options[:height] > 1
    if multi_plate
      puts "\n\nBaseplate arrangement:"
      plate_counter = 1
      (0...options[:height]).each {|y|
        (0...options[:width]).each {|x|
          print "#{plate_counter}"
          plate_counter = plate_counter + 1
        }
        print "\n"
      }
    end

    # now the arrangement of colours in each baseplate
    plate_counter = 1
    plate_size = options[:plate_size]
    (0...options[:height]).each {|plate_y|
      (0...options[:width]).each {|plate_x|
        puts "\nPlate #{plate_counter}:" if multi_plate
        plate_counter = plate_counter + 1

        (0...plate_size).each {|y|
          (0...plate_size).each {|x|
            colour = @output_image[x + (plate_size * plate_x), y + (plate_size * plate_y)]
            print "#{palette_key[colour]} "
          }
          print "\n"
        }

        print "\n"
      }
    }
  end
end


def parse_options(args)
  options = {}
  options[:input_files] = []
  options[:plate_size] = 32
  options[:width] = 3
  options[:height] = 2
  options[:palette] = :common
  options[:verbose] = false

  option_parser = OptionParser.new {|opts|
    opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

    opts.on('-i', '--input IMAGE_PNG', 'Use IMAGE_PNG as input image. Maximum 2') do |image|
      options[:input_files] << image
    end

    opts.on('-s', '--plate-size N', Integer, 'Place size (default 32)') do |size|
      options[:plate_size] = size
    end

    opts.on('-w', '--width N', Integer, 'Number of horizontal plates (default 2)') do |width|
      options[:width] = width
    end

    opts.on('-h', '--height N', Integer, 'Number of vertical plates (default 2)') do |height|
      options[:height] = height
    end

    opts.on('-p' '--palette [all|common]', 'Palette of Lego colours to use (default \'common\')') do |palette|
      case palette
        when 'all'
          options[:palette] = :all
        when 'common'
          options[:palette] = :common
        else
          puts "Valid options for palette are 'all' and 'common'\n"
          puts opts
          exit 1
      end
    end

    opts.on('-v', '--[no-]verbose', 'Run verbosely') do |v|
      options[:verbose] = v
    end
  }

  option_parser.parse!(args)


  if options[:input_files].length == 0
    puts "Must specify at least 1 input file\n"
    puts option_parser
    exit 1
  end

  if options[:input_files].length > 2
    puts "Maximum 2 input files\n"
    puts option_parser
    exit 1
  end

  options
end


def make_mosaic(args)
  options = parse_options(args)
  p options if options[:verbose]

  palette = colour_palette(options)

  mosaic1 = Mosaic.new
  mosaic1.process(options, options[:input_files][0], palette)
  mosaic1.save_output('output1.png')

  if options[:input_files].count > 1
    mosaic2 = Mosaic.new
    mosaic2.process(options, options[:input_files][1], palette)
    mosaic2.save_output('output2.png')

    joined_mosaic = Mosaic.new
    joined_mosaic.join(mosaic1, mosaic2)
    joined_mosaic.save_output('output_joined.png')
    joined_mosaic.report(options)
  else
    mosaic1.report(options)
  end
end


def lego_colours
  # Lego colours are confusing. These are closest to "brickowl" colours
  # these came from correlating data from two webpages:
  # https://www.brickowl.com/catalog/lego-slope-31-1-x-1-18862-33847-35338
  # http://ryanhowerter.net/colors
  {
      :black => 0x000000ff,
      :blue => 0x1e5aa8ff,
      :bright_green => 0x58ab41ff,
      :light_royal_blue => 0x9dc3f7ff,
      :bright_light_orange => 0xfcac00ff,
      :bright_pink => 0xff9ecdff,
      :brown => 0x7b5d41ff,
      :dark_azure => 0x469bc3ff,
      :dark_blue => 0x19325aff,
      :dark_brown => 0x372100ff,
      :dark_green => 0x00451aff,
      :dark_orange => 0x91501cff,
      :dark_pink => 0xd3359dff,
      :dark_purple => 0x441a91ff,
      :dark_red => 0x720012ff,
      :dark_stone_grey => 0x646464ff,
      :dark_tan => 0x897d62ff,
      :green => 0x00852bff,
      :light_aqua => 0xd3f2eaff,
      :lime => 0xa5ca18ff,
      :magenta => 0x901f76ff,
      :medium_azure => 0x68c3e2ff,
      :medium_blue => 0x7396c8ff,
      :medium_dark_flesh => 0xaa7d55ff,
      :medium_stone_grey => 0x969696ff,
      :olive_green => 0x77774eff,
      :orange => 0xd67923ff,
      :pearl_gold => 0xaa7f2eff,
      :red => 0xb40000ff,
      :reddish_brown => 0x5f3109ff,
      :sand_blue => 0x70819aff,
      :sand_green => 0x708e7cff,
      :white => 0xf4f4f4ff,
      :yellow => 0xfac80aff,
      :yellowish_green => 0xe2f99aff,
      :tan => 0xb0a06fff
  }
end

def colour_palette(options)
  palettes = {
      :all => lego_colours.values,
      :common => [
          # this set of "common" colours is an arbitrary list, found by
          # searching for british sellers on brickowl who have more than
          # 100 of this 1x1 slope in stock
          # https://www.brickowl.com/catalog/lego-slope-31-1-x-1-18862-33847-35338
          lego_colours[:black],
          lego_colours[:blue],
          lego_colours[:bright_light_orange],
          lego_colours[:bright_pink],
          lego_colours[:dark_blue],
          lego_colours[:dark_green],
          lego_colours[:dark_red],
          lego_colours[:dark_purple],
          lego_colours[:dark_stone_grey],
          lego_colours[:dark_tan],
          lego_colours[:green],
          lego_colours[:lime],
          lego_colours[:medium_blue],
          lego_colours[:medium_dark_flesh],
          lego_colours[:medium_stone_grey],
          lego_colours[:olive_green],
          lego_colours[:orange],
          lego_colours[:pearl_gold],
          lego_colours[:red],
          lego_colours[:reddish_brown],
          lego_colours[:sand_green],
          lego_colours[:tan],
          lego_colours[:white],
          lego_colours[:yellow],
      ]
  }

  palettes[options[:palette]]
end

make_mosaic(ARGV)
