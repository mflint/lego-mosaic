# What is this?

`mosaic` is hacky script for designing Lego mosaics from one or two png files.

# Why?

Because fun.

# How?

Start by installing the required Gem:
```
bundle install
```

Then, at its simplest, run the script like this:
```
mosaic.rb -i image.png
```

It'll produce a result image (`output1.png`), and dump a load of stuff to the console. This stuff contains a count
of the number of pieces required, broken down by colour.

By default, the image would be suitable for six 32x32 baseplates (3 horizontal by 2 vertical), and would need 6144
1x1 tiles.

# Output

The script will dump the following stuff to the console:

* a list of the colours used, along with a letter used to identify the colour in the instructions
* the arrangement of base-plates (if there's more than one)
* for each base-plate, a diagram of what colour goes where

# Options

`-i` input file (png), can be supplied once or twice (see below for discussion on _Multiple images_)

`-s` plate size - the number of dots along the size of the square base-place to use. Default 32

`-w` the number of base-plates to use horizontally. Default 3

`-h` the number of base-plates to use vertically. Default 2

`-p` the palette to use. Either `common` for a common set of Lego colours, or `all` for a larger set

`-v` verbose mode

# Multiple images

If the `-i` switch is supplied twice, two mosaics will be made, and one console report. The aim of this is to make
a _lenticular_ mosaic, which looks different depending on which direction it's viewed from. (For this to work, use [Lego
piece "Slope 31 degrees 1x1"][slope])

# Example

Here's an example using four 8x8 base-plates:

`mosaic.rb -i wyldstyle.png -s 8 -w 2 -h 2 -p all`

Output image (scaled up):

![example output image](example_output.png)

Console output:

```
Colours:
A: bright_light_orange (51)
B: dark_brown (41)
C: black (35)
D: orange (30)
E: dark_azure (16)
F: dark_orange (13)
G: dark_pink (12)
H: reddish_brown (10)
I: red (10)
J: dark_blue (7)
K: blue (6)
L: dark_red (6)
M: magenta (6)
N: yellowish_green (4)
O: yellow (3)
P: dark_stone_grey (3)
Q: brown (2)
R: dark_tan (1)


Baseplate arrangement:
12
34

Plate 1:
P D C B J K J K 
P C C C C C C B 
C C C C C J G G 
C C C L G G G B 
C C M M M B K E 
C M I C E K E H 
L I E K E B D D 
I E E B D C B D 


Plate 2:
J J H B G F B A 
J G G C E B F O 
G Q E E B B C F 
K E E B D B C B 
E B D O N F C B 
D C B A A D B B 
A A A A D D H B 
A A B B H A H B 


Plate 3:
E E B F D F D A 
E C H D D D A A 
P C H D D A D I 
Q C H D A A A I 
D F B F A A A A 
F L L C B D A A 
C C M B C L F D 
B C C M B B B B 


Plate 4:
A A B D A A H B 
A A A A A A B H 
I I A A A A B B 
I D A A A A A R 
A A A A A D A N 
A A A D F I D N 
D F F L I G O N 
B B C G G J B D 
```

[slope]: https://www.brickowl.com/catalog/lego-slope-31-1-x-1-18862-33847-35338
